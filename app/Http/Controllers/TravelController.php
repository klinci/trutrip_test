<?php

namespace App\Http\Controllers;

use App\Models\Travel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class TravelController extends Controller
{

    public function index()
    {
        $id =  auth()->user()->id;
        $data = Travel::where('user_id', '=', $id)->get();

        $response = (object)['success' => true];
        return response()->json($data, 200);
    }


    public function create(Request $request)
    {
    }


    public function store(Request $request)
    {
        $id =  auth()->user()->id;
        $validator = Validator::make($request->all(), [
            'origin_id' => 'required|integer',
            'destination_id' => 'required|integer',
            'start_schedule' => 'required|string',
            'end_schedule' => 'required|string',
            'type' => 'required|string|max:255',
            'description' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $travels = Travel::create([
            'user_id' => $id,
            'origin_id' => $request->origin_id,
            'destination_id' => $request->destination_id,
            'start_schedule' => $request->start_schedule,
            'end_schedule' => $request->end_schedule,
            'type' => $request->type,
            'description' => $request->description
        ]);

        return [
            'success' => true,
            'message' => 'Success created schedule travels.'
        ];
    }


    public function show(Travel $travel)
    {
        //
    }


    public function edit(Travel $travel)
    {
        //
    }


    public function update(Request $request, Travel $travel, $id)
    {
        $validator = Validator::make($request->all(), [
            'origin_id' => 'required|integer',
            'destination_id' => 'required|integer',
            'start_schedule' => 'required|string',
            'end_schedule' => 'required|string',
            'type' => 'required|string|max:255',
            'description' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $travel = Travel::findorfail($id);

        $travel_data = [
            'origin_id' => $request->origin_id,
            'destination_id' => $request->destination_id,
            'start_schedule' => $request->start_schedule,
            'end_schedule' => $request->end_schedule,
            'type' => $request->type,
            'description' => $request->description
        ];

        $travel->update($travel_data);

        return [
            'success' => true,
            'message' => 'Success edited schedule travels.'
        ];
    }


    public function destroy(Travel $travel, $id)
    {
        $travel = Travel::findorfail($id);
        $travel->delete();

        return [
            'success' => true,
            'message' => 'Success deleted schedule travels.'
        ];
    }
}
