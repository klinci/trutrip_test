<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\TravelController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class, 'register']);

Route::post('/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function (Request $request) {
        return auth()->user();
    });

    Route::get('/get-province', [ProvinceController::class, 'index']);
    Route::get('/get-cities/{id}', [CityController::class, 'index']);

    Route::get('/travel', [TravelController::class, 'index']);
    Route::post('/create-travel', [TravelController::class, 'store']);
    Route::post('/edit-travel/{id}', [TravelController::class, 'update']);
    Route::post('/delete-travel/{id}', [TravelController::class, 'destroy']);
    // API route for logout user
    Route::post('/logout', [AuthController::class, 'logout']);
});
