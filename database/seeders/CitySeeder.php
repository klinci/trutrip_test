<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dummy = [
            [
                "province_id" => "1",
                "name_city" => "Surabaya",
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ],
            [
                "province_id" => "1",
                "name_city" => "Malang",
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ],
            [
                "province_id" => "1",
                "name_city" => "Sidoarjo",
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ],
            [
                "province_id" => "1",
                "name_city" => "Mojokerto",
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ],
        ];

        foreach ($dummy as $data) {
            DB::table('cities')->insert([
                'province_id' => $data["province_id"],
                'name_city' => $data["name_city"],
                'created_at' => $data["created_at"],
                'updated_at' => $data["updated_at"],
            ]);
        }
    }
}
