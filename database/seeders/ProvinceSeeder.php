<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dummy = [
            [
                "id" => 1,
                "name_province" => "Jawa Timur",
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ],
        ];

        foreach ($dummy as $data) {
            DB::table('provinces')->insert([
                'id' => $data["id"],
                'name_province' => $data["name_province"],
                'created_at' => $data["created_at"],
                'updated_at' => $data["updated_at"],
            ]);
        }
    }
}
