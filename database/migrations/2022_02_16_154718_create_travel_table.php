<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travels', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('origin_id')->unsigned();
            $table->bigInteger('destination_id')->unsigned();
            $table->dateTime('start_schedule', $precision = 0);
            $table->dateTime('end_schedule', $precision = 0);
            $table->string('type');
            $table->string('description');
            $table->timestamps();
        });

        Schema::table('travels', function ($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('origin_id')->references('id')->on('cities');
            $table->foreign('destination_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel');
    }
};
