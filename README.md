## This project made using Laravel 9

## Tutorial Use
1. run composer Install
2. run php artisan migrate (for migrate database)
3. run php artisan db:seed (for data dummy required)
4. run php artisan serve

## List Url API

## Auth
- POST http://127.0.0.1:8000/api/register (for register user)
    - name string
    - email string
    - password string

    response
    {
        "data": {
            "name": "Eric Yonathan",
            "email": "eric.gamer84@gmail.com",
            "updated_at": "2022-02-15T16:48:14.000000Z",
            "created_at": "2022-02-15T16:48:14.000000Z",
            "id": 2
        },
        "access_token": "5|LCEY0NoLN7RwmQVzTgDphDjoSWBEv9I08L5r3ybE",
        "token_type": "Bearer"
    }


- POST http://127.0.0.1:8000/api/login (for login user)
    - email string
    - password string

    response
    {
        "message": "Hi Eric Yonathan, welcome to home",
        "access_token": "7|esxs8G7fAhQyF2O73HtTrC4QmHVbalQFF044CA6D",
        "token_type": "Bearer"
    }

## API GET LOCATION BASE
- GET http://127.0.0.1:8000/api/get-province (for get data province)

    response
    [
        {
            "id": 1,
            "name_province": "Jawa Timur",
            "created_at": "2022-02-15 16:15:32",
            "updated_at": "2022-02-15 16:15:32"
        }
    ]

- GET http://127.0.0.1:8000/api/get-cities/{id} (for get data cities)
    response
    [
        {
            "id": 1,
            "province_id": 1,
            "name_city": "Surabaya",
            "created_at": "2022-02-15 16:15:32",
            "updated_at": "2022-02-15 16:15:32"
        },
        {
            "id": 2,
            "province_id": 1,
            "name_city": "Malang",
            "created_at": "2022-02-15 16:15:32",
            "updated_at": "2022-02-15 16:15:32"
        },
        {
            "id": 3,
            "province_id": 1,
            "name_city": "Sidoarjo",
            "created_at": "2022-02-15 16:15:32",
            "updated_at": "2022-02-15 16:15:32"
        },
        {
            "id": 4,
            "province_id": 1,
            "name_city": "Mojokerto",
            "created_at": "2022-02-15 16:15:32",
            "updated_at": "2022-02-15 16:15:32"
        }
    ]



## API USER TRAVEL
- GET http://127.0.0.1:8000/api/profile (Get profil data)
    response
    {
        "id": 1,
        "name": "Eric Yonathan",
        "email": "eric.gamer84@gmail.com",
        "email_verified_at": null,
        "created_at": "2022-02-15T16:23:31.000000Z",
        "updated_at": "2022-02-15T16:23:31.000000Z"
    }
- GET http://127.0.0.1:8000/api/travel (Get all data travel where this user)

    response
    [
        {
            "id": 2,
            "user_id": 1,
            "origin_id": 1,
            "destination_id": 2,
            "start_schedule": "2022-02-15 18:00:00",
            "end_schedule": "2022-02-16 12:00:00",
            "type": "TRAVEL",
            "description": "Testing Data Edit",
            "created_at": "2022-02-15 16:39:08",
            "updated_at": "2022-02-15 16:42:18"
        }
    ]

- POST http://127.0.0.1:8000/api/create-travel (created schedule travel)
    - 'origin_id' => 1 [according to existing id],
    - 'destination_id' => 2 [according to existing id],
    - 'start_schedule' => 2022-02-15 18:00:00,
    - 'end_schedule' => 2022-02-16 12:00:00,
    - 'type' => 'TRAVEL',
    - 'description' => 'Testing',

    response
    {
        "success": true,
        "message": "Success created schedule travels."
    }

- POST http://127.0.0.1:8000/api/edit-travel/{id} (edited schedule travel)
    - 'origin_id' => 1 [according to existing id],
    - 'destination_id' => 2 [according to existing id],
    - 'start_schedule' => 2022-02-15 18:00:00,
    - 'end_schedule' => 2022-02-16 12:00:00,
    - 'type' => 'TRAVEL',
    - 'description' => 'Testing Edit',

    response
    {
        "success": true,
        "message": "Success edited schedule travels."
    }

- POST http://127.0.0.1:8000/api/delete-travel{id} (delete schedule travel)
    response
    {
        "success": true,
        "message": "Success deleted schedule travels."
    }
